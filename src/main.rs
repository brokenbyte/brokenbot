mod commands;
use commands::rust::*;
use commands::shell::*;

use serenity::{
    framework::standard::StandardFramework,
    model::gateway::{Activity, Ready},
    prelude::*,
};

use std::env;

struct Handler;

#[serenity::async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, _rdy: Ready) {
        ctx.set_presence(
            Some(Activity::playing("with ferris the crab")),
            serenity::model::prelude::OnlineStatus::Online,
        )
        .await;

        println!("Ready!");
    }
}

#[tokio::main]
async fn main() {
    let token: String = env::var("DISCORD_TOKEN").expect("Must set your Discord API Token!");
    let app_id = env::var("APPLICATION_ID")
        .expect("Must set your Discord Application ID!")
        .parse()
        .expect("Your Application ID is invalid!");

    let framework = StandardFramework::new()
        .configure(|c| c.prefix("&"))
        .group(&RUST_GROUP)
        .group(&SHELL_GROUP);

    let mut client = Client::builder(token)
        .framework(framework)
        .application_id(app_id)
        .event_handler(Handler)
        .await
        .unwrap();

    println!("starting");

    client.start().await.unwrap();
}
