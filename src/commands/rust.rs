use reqwest;
use reqwest::StatusCode;
use serde::Serialize;
use serde_json::Value;
use serenity::framework::standard::{macros::command, macros::group, Args, CommandResult};
use serenity::model::prelude::Message;
use serenity::prelude::*;

#[group]
#[prefixes("rust")]
#[commands(run, docs, explain)]
struct Rust;

#[allow(non_snake_case)]
#[derive(Serialize)]
struct Payload {
    channel: String,
    mode: String,
    edition: String,
    crateType: String,
    tests: bool,
    code: String,
    backtrace: bool,
}

#[command]
#[min_args(1)]
async fn run(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let code = args
        .rest()
        .trim()
        .replace("```rs", "")
        .replace("```rust", "")
        .replace("```", "");

    let payload = Payload {
        channel: String::from("stable"),
        mode: String::from("debug"),
        edition: String::from("2018"),
        crateType: String::from("bin"),
        tests: false,
        code: String::from(&code),
        backtrace: false,
    };

    let body = serde_json::to_string(&payload).unwrap();

    println!("Sending '{}'", body);

    let client = reqwest::Client::new();
    let res = client
        .post("https://play.rust-lang.org/execute")
        .json(&payload)
        .send()
        .await
        .unwrap();

    println!("\n\nGot Result '{:?}'\n\n", res);

    let res = res.text().await.unwrap();
    let res: Value = serde_json::from_str(res.as_str()).unwrap();

    let stdout = res.get("stdout").unwrap().to_string();
    let stderr = res.get("stderr").unwrap().to_string();

    // The result always has 2 quotation marks
    let stdout = if stdout.len() > 2 {
        format!(
            "```\n{}\n```",
            String::from(stdout[1..stdout.len() - 1].replace("\\n", "\n"))
        )
    } else {
        String::from("")
    };

    // The result always has 2 quotation marks
    let stderr = if stderr.len() > 2 {
        format!(
            "```\n{}\n```",
            String::from(stderr[1..stderr.len() - 1].replace("\\n", "\n"))
        )
    } else {
        String::from("")
    };

    println!("\n\nstdout\n{}\n\n", stdout);
    println!("\n\nstderr\n{}\n\n", stderr);

    msg.channel_id
        .say(ctx, format!("stdout:\n{}\nstderr:\n{}", stdout, stderr))
        .await
        .unwrap();

    Ok(())
}

#[command]
#[aliases("doc")]
#[num_args(1)]
async fn docs(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let url = format!("https://docs.rs/{}", args.rest());

    match reqwest::get(&url).await.unwrap().status() {
        StatusCode::OK => msg.channel_id.say(ctx, url).await.unwrap(),
        _ => msg
            .channel_id
            .say(
                ctx,
                "I can't find any docs with that name, are you sure you spelled it right?",
            )
            .await
            .unwrap(),
    };
    Ok(())
}
#[command]
#[num_args(1)]
async fn explain(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let error_code = args.rest().to_ascii_uppercase();

    if error_code.len() == 5
        && error_code.starts_with("E")
        && error_code.chars().skip(1).all(|c| c.is_ascii_digit())
    {
        msg.channel_id
            .say(
                ctx,
                format!("https://doc.rust-lang.org/error-index.html#{}", error_code),
            )
            .await?;
    } else {
        msg.channel_id
            .say(
                ctx,
                "That doesn't look like an error code, make sure it looks like `E1234`!",
            )
            .await?;
    }
    Ok(())
}
