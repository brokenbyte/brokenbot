use reqwest;
use serde::{Deserialize, Serialize};
use serenity::framework::standard::{macros::command, macros::group, Args, CommandResult};
use serenity::model::prelude::Message;
use serenity::prelude::*;

#[group]
#[prefixes("shell")]
#[commands(check)]
struct Shell;

#[derive(Serialize)]
struct Payload {
    script: String,
}

#[allow(non_snake_case)]
#[derive(Deserialize, Debug)]
struct Fix {
    file: String,
    line: usize,
    endLine: usize,
    column: usize,
    endColumn: usize,
    level: String,
    code: usize,
    message: String,
}

#[command]
#[min_args(1)]
async fn check(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let code = args
        .rest()
        .trim()
        .replace("```bash\n", "") // Need to eat the newline as well because shellcheck complains
        .replace("```sh\n", "")
        .replace("```zsh\n", "")
        .replace("```", "");

    let lines: Vec<&str> = code.lines().collect();

    let payload = Payload {
        script: String::from(&code),
    };

    let body = serde_json::to_string(&payload).unwrap();

    println!("Sending '{}'", body);

    let client = reqwest::Client::new();
    let res = client
        .post("https://www.shellcheck.net/shellcheck.php")
        .form(&payload)
        .send()
        .await
        .unwrap();

    println!("\n\nGot Result '{:?}'\n\n", res);

    let res = res.text().await.unwrap();
    let fixes = serde_json::from_str::<Vec<Fix>>(res.as_str()).unwrap();

    let mut lastline = 0;
    let mut padding;
    let mut response = Vec::new();
    for f in fixes {
        if f.line != lastline {
            // Print a blank on all lines but the first
            if lastline > 0 {
                response.push("\n".to_owned());
                println!("");
            }

            lastline = f.line;

            response.push(format!("line {}:", f.line));
            response.push(format!("  {}", lines[f.line - 1]));
        }

        padding = String::with_capacity(f.column);
        (0..f.column - 1).for_each(|_| padding.push(' '));
        response.push(format!("  {}^-- {}", padding, f.message));

        padding.clear();
    }

    let response = response.join("\n");

    let response = if response.is_empty() {
        String::from("```\nNo issues found!```")
    } else {
        format!("```{}```", response)
    };

    msg.channel_id.say(ctx, response).await.unwrap();

    Ok(())
}
